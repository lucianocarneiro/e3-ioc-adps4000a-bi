export IOCNAME=BI-TEST:Ctrl-IOC-001
export AS_TOP=/opt/nonvolatile
export IOCDIR=BI-TEST_Ctrl-IOC-001
export IOCNAME_SLUG=BI-TEST_Ctrl-IOC-001
export SETTINGS_FILES=settings
export LOCATION=BI-TEST
export ENGINEER="Luciano C Guedes <luciano.carneiroguedes@ess.eu>"
